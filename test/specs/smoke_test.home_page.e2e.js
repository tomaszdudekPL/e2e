import HomePage from '../pageobjects/home';
const {setWindow, openUrl} = new HomePage();

describe('Home Page. Log-in. Log-out.', () => {

  before(() => {

    setWindow({
      width: 1600,
      height: 1200
    });

    openUrl({
      baseUrl: 'https://webdriver.io'
    });

  });

  it('Should Home Page be visible.', () => {

    browser.$expect({
      msg: 'Title on Home Page is displayed.',
      el: '.projectTitle',
      visible: true
    });
    browser.$expect({
      msg: 'Fake Log-In button on Home Page is displayed.',
      el: '[href="/docs/gettingstarted.html"].button',
      visible: true
    });

    browser.$click('[href="/docs/gettingstarted.html"].button');

    browser.$expect({
      msg: '"Getting Started" is displayed.',
      el: '#__docusaurus',
      visible: true
    });

  });

});