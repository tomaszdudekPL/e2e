browser.addCommand('$click', function (element, pause = 200, index = 0) {
  $$(element)[index].click();
  browser.pause(pause);
});