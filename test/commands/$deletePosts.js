browser.addCommand('$deletePosts', function () {

  let allPostDropdownSettingsButtons = $$(s.post_three_dots_btn);
  browser.pause(1000);
  console.log(`Items to remove: ${allPostDropdownSettingsButtons.length}`);

  if(allPostDropdownSettingsButtons.length) {

    for (let i = 0; i < allPostDropdownSettingsButtons.length; i += 1) {
      allPostDropdownSettingsButtons[i].click();
      browser.$click(s.delete_post_option);
      browser.$click(s.confirm_delete_post_btn, 1000);
    }
    browser.refresh();
    browser.pause(1000);
    allPostDropdownSettingsButtons = $$(s.post_three_dots_btn);
    console.log(`Items to remove after refresh: ${allPostDropdownSettingsButtons.length}`);

    for (let i = 0; i < allPostDropdownSettingsButtons.length; i += 1) {
      allPostDropdownSettingsButtons[i].click();
      browser.$click(s.delete_post_option);
      browser.$click(s.confirm_delete_post_btn, 1000);
    }

    $(s.post_text).waitForDisplayed({ reverse: true });

  }
});