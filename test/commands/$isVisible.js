browser.addCommand('$isVisible', function (element) {
  return $(element).isDisplayed();
});