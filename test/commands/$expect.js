browser.addCommand('$expect', function ({msg, el, visible = true}) {

  if(visible) {
    expect($(el)).toBeDisplayed();
  } else {
    expect($(el)).not.toBeDisplayed();
  }
  console.log(chalk.green(`✓ Correct\n`));
});