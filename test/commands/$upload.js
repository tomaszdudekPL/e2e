browser.addCommand('$upload', function (selector, pathToFile) {

  const path = require('path');

  const fileUpload = $(selector);
  const filePath = path.join(__dirname, pathToFile);
  fileUpload.setValue(filePath);
  browser.$waitForVisible(s.photo_uploaded_ready_to_send);

});