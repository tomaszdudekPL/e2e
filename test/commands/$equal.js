browser.addCommand('$equal', function ({msg, el, expected, equal = true}) {

  if(equal) {
    expect($(el)).toHaveText(expected);
  } else {
    expect($(el)).not.toHaveText(expected);
  }
  console.log(chalk.green(`✓ Correct\n`));
});