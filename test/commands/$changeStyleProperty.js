browser.addCommand('$changeStyleProperty', function (element, cssProperty, value) {
  browser.executeScript(`$('${element}').css('${cssProperty}', '${value}')`);
});