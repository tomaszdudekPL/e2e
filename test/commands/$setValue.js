browser.addCommand('$setValue', function (element, value, pause = 200, index = 0) {
  $$(element)[index].setValue(value);
  browser.pause(pause);
});