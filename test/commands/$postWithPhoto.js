browser.addCommand('$postWithPhoto', function (numberOfPhotos) {

  browser.$click(s.new_post_placeholder);
  browser.$waitForVisible(s.new_post_textarea);

  for (let i = 0; i < numberOfPhotos; i++) {
    browser.$upload(s.photo_input_id, v.pathToImage);
    browser.pause(500);
  }

  browser.pause(1000);
  browser.$click(s.new_post_post_btn);

});