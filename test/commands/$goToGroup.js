browser.addCommand('$goToGroup', function (groupName) {

  browser.$click(s.nav_groups_btn);
  browser.$click(`[data-testid="${groupName}"]`);
  browser.$waitForVisible(s.created_group_site);

});