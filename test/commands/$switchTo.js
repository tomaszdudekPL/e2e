browser.addCommand('$switchTo', function (window, indx = 0) {

  const ids_arr = browser.getWindowHandles();

  switch (window){
    case 'last':
      indx = ids_arr.length - 1;
      break;
    case 'first':
      indx = 0;
      break;
    case 'second':
      indx = 1;
      break;
    case 'third':
      indx = 2;
      break;
  }

  browser.switchToWindow(ids_arr[indx]);

});