browser.addCommand('$createPoll', function () {

  const arrOfTexts = [
    'How are you?',
    'Who is the best?',
    'Who is the boss?',
    'Do you love Star Wars?'
  ];

  // set title of poll
  browser.$setValue(s.poll_all_inputs, v.question);

  browser.$click(s.poll_add_new_option_btn);
  browser.$click(s.poll_add_new_option_btn, 500);

  // set poll options to choose
  const allOptions = $$(s.poll_options_to_choose_input);

  allOptions.forEach((element, index) => {
    element.setValue(arrOfTexts[index]);
    browser.pause(200);
  });

  // set poll length
  browser.$click(s.select_menu_opener, 200, 0);
  browser.$click(s.poll_length_option_two_days);

  browser.$click(s.select_menu_opener, 200, 1);
  browser.$click(s.poll_length_option_one_hour);

  browser.$click(s.select_menu_opener, 200, 2);
  browser.$click(s.poll_length_option_three_minutes, 500);

});