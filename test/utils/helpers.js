const path = require('path');
const fs = require('fs');

exports.requireAll = dirpath => {
  fs.readdirSync(path.join(__dirname, '..', dirpath))
    .filter(filepath => (filepath.includes('.js')))
    .forEach(filepath => {require(path.join(__dirname, '..', dirpath) + filepath)});
};

exports.logFuncName = (color = 'red') => {
  // function to get name from inside interested function. This is hack.
  const e = new Error('dummy');
  const stack = e.stack
                 .split('\n')[2]
                 .replace(/^\s+at\s+(.+?)\s.+/g, '$1');

  console.log(chalk[color](`@ ${stack}`));
}

exports.getRandomString = () => new Date().getTime().toString(36);