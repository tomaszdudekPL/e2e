import App from './app'

export default class Navigation extends App {

  getLogout () {
    logFuncName('cyan');
    browser.pause(1000);
    browser.$click(s.nav_menu_btn);
    browser.$click(s.logout_opt);
    browser.$waitForVisible(s.memberFakeLogIn_btn);
  }

  getSettings () {
    logFuncName('cyan');
    browser.pause(1000);
    browser.$click(s.nav_menu_btn);
    browser.$click(s.settings_opt);
    browser.pause(500);
  }

}