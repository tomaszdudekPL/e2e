import HomePage from './home';

export default class Login extends HomePage {

    openLogInForm () {
        logFuncName('cyan');
        browser.$click(s.memberFakeLogIn_btn);
        browser.$waitForVisible(s.logInUsername_input);
    }

    setUserName (name) {
        logFuncName('cyan');
        browser.$setValue(s.logInUsername_input, name);
    }

    setPassword (password) {
        logFuncName('cyan');
        browser.$setValue(s.logInPassword_input, password);
    }

    submit () {
        logFuncName('cyan');
        browser.$click(s.logInSubmit_btn);
    }

    loginUser (name, password = v.pass) {
        logFuncName('red');
        browser.$waitForVisible(s.memberFakeLogIn_btn);

        this.openLogInForm();
        this.setUserName(name);
        this.setPassword(password);
        this.submit();
        browser.$waitForVisible(s.page_myworld);
        browser.$waitForVisible(s.myworld_feed);
        browser.pause(1000);
    }
}