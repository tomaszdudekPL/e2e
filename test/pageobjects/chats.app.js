import App from './app'

export default class Chats extends App {

  deleteChatThread () {
    logFuncName('cyan');
    if (browser.$isVisible(s.chat_info_btn)) {
      browser.$click(s.chat_info_btn);
      if (browser.$isVisible(s.delete_chat_btn)) {
        browser.$click(s.delete_chat_btn);
        browser.$click(s.confirm_btn);
      }
    }
    browser.pause(500);
  }

  create_new_chat (userBFullName, msg = 'MESSAGE TO DELETE') {
    logFuncName('cyan');
    browser.$click(s.create_new_chat_btn);

    const title1 = `[title="${userBFullName}"]`;
    let i = 0;
    let condition = false;

    do {
      browser.$waitForVisible(s.chat_contacts_input);
      browser.$setValue(s.chat_contacts_input, '');
      browser.pause(500);
      browser.$setValue(s.chat_contacts_input, userBFullName);
      browser.pause(1000);
      if (browser.$isVisible(title1)) {
        browser.$click(title1);
      }
      condition = browser.$isVisible(s.sg_center);
      i++;

    } while (condition && i < 5);

    browser.$setValue(s.chat_message_input, msg);
    browser.$click(s.chat_message_send_btn);
    browser.$waitForVisible(s.chat_message_text);
    browser.pause(500);

  }

}