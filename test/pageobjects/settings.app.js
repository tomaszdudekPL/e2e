import App from './app'

export default class Settings extends App {

  deleteUserAccount () {
    logFuncName('cyan');
    browser.$click(s.deleteMyAccount_btn);
    browser.$click(s.confirmDelete_checkbox);
    browser.$setValue(s.password_input, v.pass);
    browser.$click(s.delete_btn);
    browser.$click(s.submit_btn);
  }

}