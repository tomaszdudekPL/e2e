import App from './app'

export default class Events extends App {

  removeOldEvents () {
    logFuncName('cyan');

    const allEventsNames = $$(s.all_events_left_sidebar);

    for(let i = 0; i < allEventsNames.length; i++){
      allEventsNames[i].click();
      browser.$waitForVisible(s.event_name_jumbotron);
      browser.$click(s.event_settings_tab, 1000);
      browser.$click(s.delete_this_event_btn);
      browser.$click(s.delete_event_absolutely_sure_btn, 2000);
      browser.$waitForVisible(s.events_window);
    }
  }

}