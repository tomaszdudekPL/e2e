import HomePage from './home';

export default class Registration extends HomePage {

    createEmailUser ({firstName = 'tester', lastName = 'mewe', host = '@fakemail.groupl.es'}) {
        logFuncName('cyan');
        const user = {};
        const timeStamp = new Date().getTime().toString(36);
        const fullName = firstName + '-' + lastName + '_' + timeStamp;

        user.userName = fullName + host;
        user.firstName = firstName;
        user.lastName = lastName + '_' + timeStamp;
        user.timeStamp = timeStamp;
        user.fullName = fullName;
        user.password = v.pass;
        user.fullNameWithoutDash = firstName + ' ' + lastName + '_' + timeStamp;

        console.log('typeof user: ', typeof user, JSON.stringify(user, null, 4));

        return user;
    }

    createMobileUser ({firstName = 'tester', lastName = 'mewe', ordinalNumber = 1}) {
        logFuncName('cyan');
        const user = {};
        const timeStamp = new Date().getTime().toString(36);
        const getPhoneNumber = (num) => {
            const n = () => String(Math.floor(Math.random() * 9) + 1);
            return '+8900'+ num + n() + n() + n() + n() + n() + n()
        }

        user.userName = getPhoneNumber(ordinalNumber);
        user.firstName = firstName;
        user.lastName = lastName + '_' + timeStamp;
        user.timeStamp = timeStamp;
        user.password = v.pass;
        user.fullNameWithoutDash = firstName + ' ' + lastName + '_' + timeStamp;

        console.log('typeof user: ', typeof user, JSON.stringify(user, null, 4));

        return user;
    }

    registerUserAccountWithEmail () {
        logFuncName('red');

        const {firstName, lastName, userName, password} = this.createEmailUser({});

        browser.$setValue(s.reg_firstName_input, firstName);
        browser.$setValue(s.reg_lastName_input, lastName);
        browser.$setValue(s.reg_email_phone_input, userName);
        browser.$setValue(s.reg_email_phone_repeat_input, userName);
        browser.$setValue(s.reg_password_input, password);

        browser.$click(s.reg_age_checkbox);
        browser.$click(s.reg_terms_checkbox, 1000);

        browser.$click(s.reg_submit_btn);

        browser.$getFakeEmail({
            userTO: userName,
            subject: 'Quick! Confirm your email'
        });

        browser.$waitForVisible(s.reg_confirmRegistrationEmail_btn);
        browser.$click(s.reg_confirmRegistrationEmail_btn, 1500);
    }

    registerUserAccountWithPhoneNumber (ordinalNumber) {
        logFuncName('red');

        const {firstName, lastName, userName, password} = this.createMobileUser({ordinalNumber});

        browser.$setValue(s.reg_firstName_input, firstName);
        browser.$setValue(s.reg_lastName_input, lastName);
        browser.$setValue(s.reg_email_phone_input, userName);
        browser.$setValue(s.reg_email_phone_repeat_input, userName);
        browser.$setValue(s.reg_password_input, password);

        browser.$click(s.reg_age_checkbox);
        browser.$click(s.reg_terms_checkbox, 1000);

        browser.$click(s.reg_submit_btn);

        browser.$waitForVisible(s.reg_sms_code_input);
        browser.$click(s.reg_submit_btn, 1500);

    }

}