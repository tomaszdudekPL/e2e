export default class HomePage {

    openUrl ({baseUrl = 'https://master.ci.k8s.sgr-labs.com', path = ''}) {
        logFuncName('cyan');
        browser.$url(`${baseUrl}/${path}`);
    }

    setWindow ({width = '1600', height= '1200'}) {
        logFuncName('cyan');
        browser.$setWindowSize(width, height);
    }
}