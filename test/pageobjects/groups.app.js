import App from './app'

export default class Groups extends App {

  removeOldGroups () {
    logFuncName('cyan');
    const allGroups = $$(s.all_groups_left_sidebar);

    for(let i = 0; i < allGroups.length; i++){

      browser.$waitForVisible(s.page_myworld);

      allGroups[i].click();
      browser.$click(s.group_settings_tab);
      browser.$click(s.delete_this_group_btn);
      browser.$setValue(s.delete_group_password_input, v.pass);
      browser.$click(s.delete_group_input_delete_btn);

      browser.$waitForVisible(s.page_myworld);
      browser.$click(s.nav_groups_btn);
    }
  }

}