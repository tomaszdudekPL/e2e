To start tests you need to have Node 12 installed.

When repo is cloned:

```npm install```

When dependencies are ready you can start test:

```npm start```